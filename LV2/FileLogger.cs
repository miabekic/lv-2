﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class FileLogger: ILogger
    {
        private string filePath;
        public FileLogger(string filePath)
        {
            this.filePath = filePath;
        }
        
        //public void Log(string message)       /*ZADATAK 4.*/
        public void Log(ILogable data)             /*ZADATAK 5.*/
        {

            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
           {
                //writer.WriteLine(message);            /*ZADATAK 4.*/ 
                writer.WriteLine(data.GetStringRepresentation());           /*ZADATAK 5.*/
            }

        }
        
    }
}
