﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Die
    {

        private int numberOfSides;
        /*private Random randomGenerator;*/                      /*POCETNO ZADANO, ZADATAK 1.,2.*/
        private RandomGenerator randomGenerator;                          /*ZADATAK 3.*/
        public Die(int numberOfSides/*, Random randomGenerator*/)   /*ZADATAK 2. (DODATNI ARGUMENT)*/
        {
            this.numberOfSides = numberOfSides;
            /*this.randomGenerator = new Random();*/                 /*POCETNO ZADANO, ZADATAK 1.*/
            /*this.randomGenerator = randomGenerator;  */            /*ZADATAK 2.*/
            randomGenerator = RandomGenerator.GetInstance();               /*ZADATAK 3.*/
        }

        public int Roll()
        {
            /*int rolledNumber = randomGenerator.Next(1, numberOfSides + 1); */         /*POCETNO ZADANO, ZADATAK 1.,2.*/
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);                 /*ZADATAK 3.*/
            return rolledNumber;
        }

        //ZADATAK 7.
        public int getNumberOfSides
        {
            get { return numberOfSides; }
        }




    }
}