﻿using System.Collections.Generic;
using System;

namespace LV2
{
    class FlexibleDiceRoller : IHand, IRollingMachine
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        //ZADATAK 7.
        public void RemoveSpecificDice(int numberOfSides)
        {
            List<int> indexesOfRemovedDice = new List<int>();
            for (int i = 0; i < dice.Count; i++)
            {
                if (dice[i].getNumberOfSides == numberOfSides)
                {
                    dice.RemoveAt(i);
                    i--;
                }
            }
        }
        public void printNumberOfSides()
        {
            for (int i = 0; i < dice.Count; i++)
            {
                Console.WriteLine(dice[i].getNumberOfSides);
            }
        }

    }
}
