﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{

    class ConsoleLogger:ILogger
    {
        public ConsoleLogger() { }

        //public void Log(string message)           /*ZADATAK 4.*/     
        public void Log(ILogable data)              /*ZADATAK 5.*/
        {
            //Console.WriteLine(message + "\n");        /*ZADATAK 4.*/ 
            Console.WriteLine(data.GetStringRepresentation());     /*ZADATAK 5.*/
        }

    }
}
