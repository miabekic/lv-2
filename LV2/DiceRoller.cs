﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class DiceRoller : ILogable /*ZADATAK 5.(SUCELJE)*/
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private ILogger iLogger;   /*ZADATAK 4*/

        public DiceRoller(ILogger ilogger)    /*ZADATAK 4.(argument)*/
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.iLogger = ilogger;           /*ZADATAK 4.*/
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
        public void printResultsOfRolling()
        {
            for (int i = 0; i < DiceCount; i++)
            {
                Console.WriteLine(resultForEachRoll[i]);
            }
        }

        //ZADATAK 4.
        //public void LogRollingResults()
        //{
        //    StringBuilder stringBuilder = new StringBuilder();
        //    foreach (int result in this.resultForEachRoll)
        //    {
        //        stringBuilder.Append(result.ToString());
        //        iLogger.Log(stringBuilder.ToString());
        //    }
        //}

        //ZADATAK 5.
        public string GetStringRepresentation()
        {
            StringBuilder buildedResults = new StringBuilder();
            for (int i = 0; i < DiceCount; i++)
            {
                buildedResults.Append(resultForEachRoll[i] + "\n");
            }
            return buildedResults.ToString();
        }
    }
  
}
